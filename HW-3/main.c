//
//
//  Home Work 3
//
//  Daria Shumova
//

#include <stdio.h>
#include <mm_malloc.h>

typedef struct
{
    int *ptr;
    u_int size;
} arrayInt;

void swapXor (int *a, int *b)
{
    *a = *a ^ *b;
    *b = *a ^ *b;
    *a = *a ^ *b;
}

//------------- 1. Обычная сортировка пузырьком ----------------
int bubbleSort (arrayInt *array)
{
    int counter = 0;
    
    if (!array->size || !array->ptr)
        printf("Массив пустой\n");
    else
    {
        for (int i = 0; i < array->size; i++)
            for (int j = 0; j < array->size - 1; j++)
            {
                if (array->ptr[j] > array->ptr[j+1])
                    swapXor(&array->ptr[j], &array->ptr[j+1]);
                counter++;
            }
    }
    return counter;
}

//------------- 1. Улучшенная сортировка пузырьком ----------------
int bubbleSortImproved (arrayInt *array)
{
    int counter = 0;
    
    if (!array->size || !array->ptr)
        printf("Массив пустой\n");
    else
    {
        int flag = 1;
        
        for (int i = array->size; i && flag; i--)
        {
            flag = 0;
            for (int j = 0; j < i - 1; j++)
            {
                if (array->ptr[j] > array->ptr[j+1])
                {
                    swapXor(&array->ptr[j], &array->ptr[j+1]);
                    flag = 1;
                }
                counter++;
            }
        }
    }
    return counter;
}

//--------------- 2. Шейкерная сортировка -------------------
int shakerSort (arrayInt *array)
{
    int counter = 0;
    
    if (!array->size || !array->ptr)
        printf("Массив пустой\n");
    else
    {
        int flag;
        int start = 0;
        int finish = array->size - 1;
    
        do
        {
            flag = 0;
            for (int j = start; j < finish; j++)
            {
                if (array->ptr[j] > array->ptr[j+1])
                {
                    swapXor(&array->ptr[j], &array->ptr[j+1]);
                    flag = j;
                }
                counter++;
            }
            finish = flag;
            
            for (int j = finish; j > start; j--)
            {
                if (array->ptr[j] < array->ptr[j-1])
                {
                    swapXor(&array->ptr[j], &array->ptr[j-1]);
                    flag = j;
                }
                counter++;
            }
            start = flag;
        }
        while (start != finish && flag);
    }
        
    return counter;
}

//----------------- 3. бинарный алгоритм поиска ---------------
int binarySearch (arrayInt *array, int x)
{
    int result = -1;
    
    if (!array->size || !array->ptr)
        printf("Массив пустой\n");
    else
    {
        int start = 0;
        int finish = array->size - 1;
        int mid;

        do
        {
            mid = start + (finish - start) / 2;
            if (x == array->ptr[mid])
                result = mid;
            else if (x > array->ptr[mid])
                start = mid;
            else
                finish = mid;
        }
        while ((start != finish) && result < 0);
    }
        return result;
}

//-------------- main ------------------
int main(int argc, const char * argv[]) {
    
    arrayInt myArray = {.ptr = NULL, .size = 10};
    int counter;
    int x;
    
    myArray.ptr = (int *) malloc(myArray.size * sizeof(int));
    myArray.ptr[0] = 1;
    myArray.ptr[1] = 7;
    myArray.ptr[2] = 10;
    myArray.ptr[3] = 2;
    myArray.ptr[4] = 4;
    myArray.ptr[5] = 9;
    myArray.ptr[6] = 8;
    myArray.ptr[7] = 5;
    myArray.ptr[8] = 6;
    myArray.ptr[9] = 3;
    
    printf("Массив: ");
    for (int i = 0; i < myArray.size; i++)
        printf("%i ", myArray.ptr[i]);
    
    printf("\nКакой алгоритм сортировки использовать?\nПузырек - 1\nУлучшенный пузырек - 2\nШейкерная сортировка - 3\n");
    scanf("%i", &x);
    while ((x < 1) || (x > 3))
    {
        printf("Неверная команда, введите 1, 2 или 3\n");
        scanf("%i", &x);
    }
    
    switch (x)
    {
        case 1:
            counter = bubbleSort(&myArray);
            break;
        case 2:
            counter = bubbleSortImproved(&myArray);
            break;
        default:
            counter = shakerSort(&myArray);
            break;
    }
    
    printf("Отсортированный массив: ");
    
    for (int i = 0; i < myArray.size; i++)
        printf("%i ", myArray.ptr[i]);
    
    printf("\nСортировка заняла %i операций сравнения\n", counter);
    
    printf("Введите число, которое необходимо найти (от 1 до 10): ");
    scanf("%i", &x);
    
    while (x < 1 || x > 10)
    {
        printf("Число должно быть от 1 до 10, попробуйте еще раз: \n");
        scanf("%i\n", &x);
    }
    
    printf("Индекс искомого числа: %i\n", binarySearch(&myArray, x));

    return 0;
}
